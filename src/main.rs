use std::env;
use std::process::Command;
use std::fs;
use std::fs::File;
use std::io::prelude::*;

const MAX_TEMP: i32 = 82_000;//Critical temp and we need to reboot-
const MAX_REBOOT_COUNT: i32 = 10; //Maximum numbers of reboot allowed before shutdown
const REBOOT_FILE: &str = "reboot_count.txt";

fn main() {
    let mut reboot_count = 0;
    let mut in_danger = false;

    if let Ok(contents) = fs::read_to_string(get_reboot_file_path()) {
        let contents = contents.trim();
        if let Ok(num) = contents.parse::<i32>() {
            reboot_count = num;
        }
    }


    for i in 0..4 {
        let path = format!("/sys/class/thermal/thermal_zone{}/temp", i);

        if let Ok(contents) = fs::read_to_string(&path) {
            let contents = contents.trim();

            if let Ok(num) = contents.parse::<i32>() {

                println!("TEMP {}", num);
                if num >= MAX_TEMP {
                    in_danger = true;
                    println!("DANGER");
                }
            }
        }
    }

    if in_danger {
        if reboot_count < MAX_REBOOT_COUNT {
            println!("Rebooting");

            if let Ok(mut file) = File::create(get_reboot_file_path()) {
                reboot_count += 1;
                file.write_all(reboot_count.to_string().into_bytes().as_slice()).unwrap();
            }

            if !cfg!(debug_assertions) {
                    let _ = Command::new("reboot").status();
            }
        } else if reboot_count >= MAX_REBOOT_COUNT {
            println!("Shutting down");
            if let Ok(mut file) = File::create(get_reboot_file_path()) {
                file.write_all("0".to_string().into_bytes().as_slice()).unwrap();
            }

            if !cfg!(debug_assertions) {
                let _ = Command::new("shutdown").arg("-h").arg("now").status();
            }
        }
    }
}

fn get_reboot_file_path() -> String {
    let exe = env::current_exe().unwrap();
    let parent = exe.parent().unwrap();
    let reboot_path = parent.join(REBOOT_FILE);
    reboot_path.into_os_string().into_string().unwrap()
}
